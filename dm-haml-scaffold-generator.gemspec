# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|

  s.name          = 'dm-haml-scaffold-generator'
  s.version       = '0.0.4'
  s.authors       = ['BM5k']
  s.date          = '2010-01-19'
  s.email         = 'gems@bm5k.com'
  s.homepage      = 'http://github.com/bm5k/dm-haml-scaffold-generator'
  s.rdoc_options  = ['--charset=UTF-8']
  s.summary       = 'Generate DataMapper/RSpec/HAML scaffolds with partials. Ported from rspec-haml-scaffold-generator by Zach Inglis & Daniel Fischer'

  s.extra_rdoc_files = [
    'LICENSE',
    'README.rdoc'
  ]

  s.files = [
    'LICENSE',
    'README.rdoc',
    'VERSION.yml',
    'generators/USAGE',
    'generators/dm_haml_scaffold/dm_haml_scaffold_generator.rb',
    'generators/dm_haml_scaffold/templates/INSTALL',
    'generators/dm_haml_scaffold/templates/controller.rb',
    'generators/dm_haml_scaffold/templates/edit_haml_spec.rb',
    'generators/dm_haml_scaffold/templates/helper.rb',
    'generators/dm_haml_scaffold/templates/helper_spec.rb',
    'generators/dm_haml_scaffold/templates/index_haml_spec.rb',
    'generators/dm_haml_scaffold/templates/model.rb',
    'generators/dm_haml_scaffold/templates/model_spec.rb',
    'generators/dm_haml_scaffold/templates/view__form_haml.erb',
    'generators/dm_haml_scaffold/templates/view__singular_haml.erb',
    'generators/dm_haml_scaffold/templates/new_haml_spec.rb',
    'generators/dm_haml_scaffold/templates/show_haml_spec.rb',
    'generators/dm_haml_scaffold/templates/view_edit_haml.erb',
    'generators/dm_haml_scaffold/templates/view_index_haml.erb',
    'generators/dm_haml_scaffold/templates/view_new_haml.erb',
    'generators/dm_haml_scaffold/templates/view_show_haml.erb'
  ]

end
