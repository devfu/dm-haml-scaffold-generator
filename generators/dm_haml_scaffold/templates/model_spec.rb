require File.dirname(__FILE__) + '/../spec_helper'

describe <%= model_name %> do

  before :each do
    @<%= singular_name %> = <%= model_name %>.gen
  end

  it 'should be valid' do
    @<%= singular_name %>.should be_valid
  end

end
