class <%= controller_class_name %>Controller < ApplicationController

  # GET /<%= plural_name %>
  # GET /<%= plural_name %>.xml
  def index
    @<%= plural_name %> = <%= model_name %>.all

    respond_to do |format|
      format.html # index.html.haml
      format.xml  { render :xml => @<%= plural_name %> }
    end
  end

  # GET /<%= plural_name %>/1
  # GET /<%= plural_name %>/1.xml
  def show
    @<%= singular_name %> = <%= model_name %>.get params[:id]

    respond_to do |format|
      format.html # show.html.haml
      format.xml  { render :xml => @<%= singular_name %> }
    end
  end

  # GET /<%= plural_name %>/new
  # GET /<%= plural_name %>/new.xml
  def new
    @<%= singular_name %> = <%= model_name %>.new

    respond_to do |format|
      format.html # new.html.haml
      format.xml  { render :xml => @<%= singular_name %> }
    end
  end

  # GET /<%= plural_name %>/1/edit
  def edit
    @<%= singular_name %> = <%= model_name %>.get params[:id]
  end

  # POST /<%= plural_name %>
  # POST /<%= plural_name %>.xml
  def create
    @<%= file_name %> = <%= model_name %>.new params[:<%= singular_name %>]

    respond_to do |format|
      if @<%= file_name %>.save
        flash[:notice] = '<%= model_name %> was successfully created!'
        format.html { redirect_to <%= table_name.singularize %>_path(@<%= file_name %>) }
        format.xml  { render :xml => @<%= file_name %>, :status => :created, :location => @<%= file_name %> }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @<%= file_name %>.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /<%= plural_name %>/1
  # PUT /<%= plural_name %>/1.xml
  def update
    @<%= file_name %> = <%= model_name %>.get params[:id]

    respond_to do |format|
      if @<%= file_name %>.update_attributes params[:<%= file_name %>]
        flash[:notice] = '<%= singular_name.capitalize %> was successfully updated!'
        format.html { redirect_to <%= table_name.singularize %>_path(@<%= file_name %>) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @<%= file_name %>.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /<%= plural_name %>/1
  # DELETE /<%= plural_name %>/1.xml
  def destroy
    @<%= file_name %> = <%= model_name %>.get params[:id]
    @<%= file_name %>.destroy

    respond_to do |format|
      format.html { redirect_to <%= table_name %>_url }
      format.xml  { head :ok }
    end
  end

end
